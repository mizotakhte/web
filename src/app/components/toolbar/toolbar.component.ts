import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {OidcSecurityService} from "angular-auth-oidc-client";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private oidcSecurityService: OidcSecurityService) { }

  @Output() sidenavToggle = new EventEmitter<void>();

  ngOnInit(): void {
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  logout() {
    this.oidcSecurityService.logoffAndRevokeTokens().subscribe(result => console.info(result));
  }
}
